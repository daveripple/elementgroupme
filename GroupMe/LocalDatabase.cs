using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;

namespace GroupMe
{
  public class LocalDatabase : IDisposable
  {
    private string mySqlConnectionString = "";

    public SqlConnection myConnectionLocal;
    public SqlCommand myCommandLocal;
    public SqlDataReader myReaderLocal = null;
    public SqlTransaction myTransLocal = null;

    public LocalDatabase()
    {
     // initialize(mySqlConnectionString);
     initialize("Data Source=localhost;Initial Catalog=Element;User ID=toby;Password=toby;");
    }


    public LocalDatabase(string connectionString)
    {
      initialize(connectionString);
    }

    private void initialize(string connectionString)
    {
      try
      {
        myConnectionLocal = new SqlConnection(connectionString);
        myConnectionLocal.Open();
        myCommandLocal = myConnectionLocal.CreateCommand();
        myCommandLocal.Connection = myConnectionLocal;
        myReaderLocal = null;
      }
      catch (Exception e)
      {
        Console.WriteLine("BIG ERROR DB:" + e.Message);
      }
    }

    public long insertWithIdentity(string sql)
    {

      long retVal = 0;

      sql += " SELECT SCOPE_IDENTITY()";

      try
      {
        string sqlstring = sql;
        myCommandLocal.CommandText = sqlstring;
        retVal = Convert.ToInt64(myCommandLocal.ExecuteScalar());
      }
      catch (Exception e)
      {

      }

      return retVal;
    }


    public void insertOrUpdate(string sql)
    {
      try
      {
        string sqlstring = sql;
        myCommandLocal.CommandText = sqlstring;
        myCommandLocal.ExecuteNonQuery();
      }
      catch (Exception e)
      {

      }

    }

    public DataTable SelectDataTable(SqlCommand command)
    {
      DataTable dataTable = null;
      try
      {
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
        sqlDataAdapter.SelectCommand = command;
        dataTable = new DataTable();
        sqlDataAdapter.Fill(dataTable);
      }
      catch (Exception e)
      {
        string debug = command.CommandText;
        Console.WriteLine(e.Message);
        // throw e;//
      }

      return dataTable;
    }

    void IDisposable.Dispose()
    {
      try
      {
        myConnectionLocal.Close();
      }
      catch (Exception e)
      { }
    }
  }
}
