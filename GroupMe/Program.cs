using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace GroupMe
{
    class Program
    {
        static void Main(string[] args)
        {
            runURL("https://api.groupme.com/v3/groups/39974178?token=tYYPJZl17JvNREYLDUuFQsEgFG5BVJC9t8yzdS8T", 1);
            runURL("https://api.groupme.com/v3/groups/3178974?token=tYYPJZl17JvNREYLDUuFQsEgFG5BVJC9t8yzdS8T", 2);
            runURL("https://api.groupme.com/v3/groups/3392197?token=tYYPJZl17JvNREYLDUuFQsEgFG5BVJC9t8yzdS8T", 3);
            runURL("https://api.groupme.com/v3/groups/3978544?token=tYYPJZl17JvNREYLDUuFQsEgFG5BVJC9t8yzdS8T", 4);
            runURL("https://api.groupme.com/v3/groups/48735102?token=tYYPJZl17JvNREYLDUuFQsEgFG5BVJC9t8yzdS8T", 5);
            runURL("https://api.groupme.com/v3/groups/51464157?token=tYYPJZl17JvNREYLDUuFQsEgFG5BVJC9t8yzdS8T", 6);
        }


        private static void runURL(string url, int groupID)
        {

          using (LocalDatabase ldb = new LocalDatabase())
          {
            ldb.myCommandLocal.CommandText = "delete from groupMeUsers  where groupID = " + groupID;
            ldb.myCommandLocal.ExecuteNonQuery();
          }

          using (WebClient wc = new WebClient())
          {
              var json = wc.DownloadString(url);

              List<string> names = new List<string>(); 

              var jsonReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);

              string group = jsonReturn.response.name.ToString();


              foreach (var member in jsonReturn.response.members)
              {
                  string memberName = member.name.ToString();
                  names.Add(memberName);

                  using (LocalDatabase ldb = new LocalDatabase())
                  {
                    ldb.myCommandLocal.CommandText = "insert into groupmeUsers(name, groupID) values('" + memberName.Replace("'","''") +"'," + groupID +")";
                    ldb.myCommandLocal.ExecuteNonQuery();
                  }

                  using (LocalDatabase ldb = new LocalDatabase())
                  {
                    ldb.myCommandLocal.CommandText = "insert into groupmeHistory(name, groupID) values('" + memberName.Replace("'", "''") + "'," + groupID + ")";
                    ldb.myCommandLocal.ExecuteNonQuery();
                  }

              }

              /*
              using (var file = File.CreateText(group + ".csv"))
              {
                  foreach (var arr in names)
                  {
                      file.WriteLine(string.Join(",", arr));
                  }
              }*/

          }
        }
    }
}
